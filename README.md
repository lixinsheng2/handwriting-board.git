# handwriting-board

## 介绍

此项目为手写板打样demo，该项目是手写板应用、手写版驱动合一项目，建议手写板应用与手写板驱动分开项目管理更清晰。  

手写板应用主要功能：

- 设备连接状态显示（查询、绑定设备驱动）
- 手写板按键设置
- 手写板区域设置
- 退出（解绑设备驱动）

手写板驱动应用主要功能：

- 与设备进行USB通信（获取设备输入）
- 与系统进行通信（向系统写入事件）

## 软件架构

![驱动架构图](./architecture.png)  
- 扩展外设管理服务：主要职责设备管理，驱动包管理
- 扩展外设应用：主要职责对驱动进行自定义设置，包括按键、区域等
- 外设扩展驱动（应用）：主要职责与系统及设备之间通信，将用户操作体现到系统上

## 工程目录

```text
entry/src/main/ets
|---components
|   |---AboutComponent.ets                 //“关于”部件页面构造
|   |---KeyPressComponent.ets              //“按键设置”部件页面构造
|   |---PensComponent.ets                  //“笔”部件页面构造
|   |---RegionComponent.ets                //“区域设置”部件页面构造
|---DriverAbility
|   |---driver.ts                          //主要重载了继承驱动扩展能力DriverExtensionAbility，Onit初始化USB接口等
|---entryability
|---pages
|   |---Index.ets                          //应用首页
|---tool
|   |---RpcTool.ets                        //主要封装了扩展外设相关接口，实现相关功能，如：查询设备，绑定设备，连接远程对象等
entry/src/main/cpp
|---types
|   |---libentry                           //创建native c++工程，自动生成的文件目录
|---data_parser.cpp                        //主要封装了手写笔和按键相关接口，实现相关功能，如：笔的移动或者按下，按键的按下或者抬起等功能
|---data_parser.h                          //定义DataParser类，声明相关接口和成员变量
|---hello.cpp                              //主要封装了手写板驱动相关接口，实现相关功能，如：获取键盘输入，设置方向，获取方向等功能
|---hid_simulator.cpp                      //主要封装了转换事件数据相关接口，实现相关功能，如：转换移动事件数据，转换按键事件数据等功能
|---hid_simulator.h                        //定义了HidSimulator类，声明了数据类型EventType，EventData
|---inject_thread.cpp                      //主要封装了线程相关接口，实现相关功能，如：创建触摸屏设备，创建键盘设备等相关功能
|---inject_thread.h                        //定义了InjectThread类，声明了相关接口和成员变量，定义了虚拟设备名称
|---key_code.h                             //定义了数据类型KeyCode，声明了支持的按键值
|---parse_point_info.cpp                   //实现了接口ParsePointInfo，主要功能：手写笔坐标转换
|---parse_point_info.h                     //声明接口ParsePointInfo
```

## 关键代码讲解

### 手写板应用关键代码

1. 在Index.ets中，页面加载时初始化RpcTool。

    ```
    async aboutToAppear() {
        hilog.info(0, 'testTag ui', 'aboutToAppear enter')
        RpcTool.getInstance().init(this.bindCallback)
        RpcTool.getInstance().bindDevice()
      }
    ```
2. 在RpcTool.ets中查询设备列表，并根据vorderId，productId匹配设备。

    * 引包

    ```
    import { deviceManager } from '@kit.DriverDevelopmentKit';
    import { rpc } from '@kit.IPCKit';
    ```

    * 查询设备列表

    ```
    private query() {
      hilog.info(0, 'testTag ui', 'query enter');
      if (this.isBinding || this.bindState) {
        hilog.info(0, 'testTag ui', 'has bind. device id:' + this.deviceId)
        return
      }
      if (this.deviceId != -1) {
        hilog.info(0, 'testTag ui', 'has query. device id:' + this.deviceId)
        return;
      }
      this.isQuerying = true
      try {
        let devices: Array<deviceManager.Device>  = deviceManager.queryDevices(deviceManager.BusType.USB);
        for (let item of devices) {
          let device = item as deviceManager.USBDevice;
          hilog.info(0, 'testTag ui', 'querydevice id:' + device.deviceId + ',bustype:' + device.busType
          + ',vid:' + device.vendorId + ',pid:' + device.productId + ', des:' + device.description);
          let index = PRODUCT_ID_LIST.indexOf(device.productId)
          if (index >= 0) {
            this.deviceId = device.deviceId;
            this.vendorId = device.vendorId;
            this.productId = device.productId;
            this.description = device.description;
            break;
          }
        }
      } catch (error) {
        hilog.error(0, 'testTag ui', `Failed to query device. Code is ${error.code}, message is ${error.message}`);
      }
      this.message = this.deviceId.toString();
      this.isQuerying = false
    }
    ```
   
3. 在RpcTool.ets绑定设备。

    ```
    private async bind() {
      try {
        hilog.info(0, 'testTag ui', 'bindDevice id is:' + this.deviceId);
        // 通过ArkTs接口通过设备Id将设备绑定到设备驱动上，可以获取到设备驱动的远程对象
        let data: deviceManager.RemoteDeviceDriver = await deviceManager.bindDeviceDriver(this.deviceId,
        async (err: BusinessError, id: number) => {
          hilog.error(0, 'testTag ui', 'Device is disconnected:' + data);
          this.bindState = false;
          this.isBinding = false;
          this.bindStateChanged()
        });

        hilog.info(0, 'testTag ui', 'bindDevice success:' + data.deviceId);
        // 获取到设备驱动的远程对象，可以通过远程对象与设备驱动进行通信（配置驱动参数）
        this.remote = data.remote as rpc.RemoteObject;
        if (this.remote === null) {
          hilog.error(0, 'testTag ui', 'create remote fail');
          this.bindState = false;
          this.isBinding = false;
          return;
        }
        this.bindState = true;
        this.isBinding = false;
        hilog.info(0, 'testTag ui', 'create remote success');
      } catch (error) {
        hilog.error(0, 'testTag ui', 'bindDevice fail. Code:' + error + ', message:' + error.message);
        this.bindState = false;
        this.isBinding = false;
      }
    }
    ```

4. 在RpcTool.ets根据用户选择，与驱动进行IPC通信。

    ```
    setKeyValue(keyIndex: number, keyValue: number) {
      // 直接调用rpc的接口向服务端发送消息，客户端需自行对入参进行序列化，对返回值进行反序列化，操作繁琐
      let option = new rpc.MessageOption();
      let dataSend = new rpc.MessageSequence();
      let reply = new rpc.MessageSequence();
      dataSend.writeInt(keyIndex);
      dataSend.writeInt(keyValue);
      hilog.info(0, 'testTag ui', `send key value to remote keyIndex:${keyIndex} keyValue:${keyValue}`);
      if (this.remote == null) {
        hilog.error(0, 'testTag ui', `the remote is null`)
        return;
      }
      // 通过驱动的远程对象与设备驱动进行通信，开发者需约定好应用侧与驱动侧的通信规则
      this.remote.sendMessageRequest(REQUEST_CODE_KEY_VALUE, dataSend, reply, option).then((ret) => {
        let msg = reply.readInt();
        hilog.info(0, 'testTag ui', `setKeyValue ret:${ret} msg:${msg}`);
      }).catch((error) => {
        hilog.info(0, 'testTag ui', 'setKeyValue failed');
      });
    }
    ```
    
### 手写板驱动关键代码
1. 根据设备实际情况配置驱动配置文件，entry/src/main/module.json5，在module节点下配置extensionAbilities，关键属性包括实现DriverExtensionAbility实体位置（srcEntry），类型为driver（type），总线类型为USB（metadata节点下bus），产品PID（metadata节点下pid），产品VID（metadata节点下vid）等

    ```
    "extensionAbilities": [
      {
        "name": "DriverAbility",
        "srcEntry": "./ets/DriverAbility/driver.ts",
        "icon": "$media:app_icon",
        "exported": true,
        "type": "driver",
        "description": "extensionDescription",
        "metadata": [
          {
            "name": "bus", // 必填项，所属总线
            "value": "USB"
          },
          {
            "name": "desc", // 必填项，必要的驱动描述
            "value": "the sample of driverExtensionAbility"
          },
          {
            "name": "vendor", // 必填项，驱动厂商名称
            "value": "string"
          },
          {
            "name": "vid", // 支持 USB vendor id 列表，用逗号分隔，不可为空
            "value": "0x0B57"
          },
          {
            "name": "pid", // 支持的 USB product id 列表，用逗号分隔，不可为空
            "value": "0xA13F,0xA150"
          }
        ]
      }
    ]
    ```

2. 继承驱动扩展能力DriverExtensionAbility，Onit初始化USB接口。

    * 引包

    ```
    // 导入扩展驱动外设元能力
    import Extension from '@ohos.app.ability.DriverExtensionAbility';
    
    ```

    * 驱动类

    ```
    export default class DriverExtAbility extends Extension {
        // 获取自定义按键的映射关系
        getKeyMapInfo() {
            let filePath = this.context.filesDir;
            let keyStorage = dataStorage.getStorageSync(filePath + '/keyInfo');
            STORAGE_KEY.forEach((value, index) => {
                defaultIndexMap[index] = keyStorage.getSync(value.key, value.default)
            })
        }
    
        // 获取手写板自定义的输入方向
        getDirectionInfo() {
            let filePath = this.context.filesDir;
            let keyStorage = dataStorage.getStorageSync(filePath + '/keyInfo');
            direction = keyStorage.getSync('direction', 0);
        }
    
        // 当有设备插入到系统，扩展外设框架会通过vId与pId匹配到外设驱动包，匹配到后会通过系统元能力会拉起扩展外设驱动元能力，元能力拉起来便会调用onInit
        onInit(want): void {
            deviceId = want.parameters["deviceId"];
            // 初始化USB接口，并获取描述信息
            let ret = testNapi.getProductDesc(want.parameters["deviceId"]);
            if (ret != undefined) {
                [globalThis.productDesc, globalThis.productId] = ret;
            }
            this.getKeyMapInfo()
            this.getDirectionInfo()
            // 获取屏幕分辨率
            display.getAllDisplays((err, data:Array<display.Display>) => {
                hilog.info(0, 'testTag', 'getAllDisplays callback');
                if (err.code) {
                    testNapi.setDisplayInfo(2160, 1440);
                } else {
                    testNapi.setDisplayInfo(data[0].width, data[0].height);
                }
                // 传递给native层
                testNapi.setButtonMapping(defaultIndexMap);
                testNapi.setDirection(direction);
                GetData();
                //PowerManagwer();
            });
            globalThis.context = this.context;
    
        }
        // 外设驱动元能力释放
        onRelease() :void {
            hilog.info(0, 'testTag', 'DriverAbility onRelease');
            // 释放底层资源
            testNapi.releaseResource();
        }
        // 外设驱动元能力被连接
        onConnect(want): rpc.RemoteObject {
            hilog.info(0, 'testTag', 'DriverAbility onConnect');
            globalThis?.connectStatus = true;
            let robj = new FirstDriverAbilityStub("remote")
            return robj;
        }
        // 外设驱动元能力断开连接
        onDisconnect(want): void {
            hilog.info(0, 'testTag', 'DriverAbility onDisconnect');
            globalThis?.connectStatus = false;
        }
        onDump(params): Array<string> {
            hilog.info(0, 'testTag', 'DriverAbility onDump, params:' + JSON.stringify(params));
            return ['params'];
        }
    };
    ```

    * 与应用远程IPC Stub类

    ```
    // 驱动侧，与应用侧通信的远程对象
    class FirstDriverAbilityStub extends rpc.RemoteObject {
        constructor(des) {
            if (typeof des === 'string') {
                super(des);
            } else {
                return null;
            }
        }
        // 与应用侧约定好的通讯规则
        onRemoteMessageRequest(code: number,
            data: MessageSequence,
            reply: MessageSequence,
            options: MessageOption
        ) {
            hilog.info(0, 'testTag', `testtag driver extension onRemoteMessageRequest called ${code}`);
            try{
                // code等于1，表示设置自定义按键映射关系
                if (code == 1) {
                    let keyIndex = data.readInt();
                    let keyValue = data.readInt();
                    console.info(`testtag driver extension start setOneButtonMapping key:${keyIndex} val:${keyValue}`);
                    testNapi.setOneButtonMapping(keyIndex, keyValue);
                    console.info(`testtag driver extension setOneButtonMapping called key:${keyIndex} val:${keyValue}`);
                    reply.writeInt(keyIndex);
                    return true;
                } else if (code == 2) {
                    // code等于2，表示设置自定义方向
                    let direction = data.readInt();
                    console.info(`testtag driver start setDirection direction:${direction}`);
                    testNapi.setDirection(direction);
                    console.info(`testtag driver setDirection called`);
                    reply.writeInt(direction);
                    return true;
                } else if (code == 3) {
                    // code等于2，表示连接状态变更
                    let type = data.readInt();
                    type = (globalThis?.connectStatus ? 1 : 0);
                    hilog.info(0, 'testTag', `testtag get connection status called status:${type}`);
                    reply.writeInt(type);
                    return true;
                }
            } catch (error) {
                hilog.info(0, 'testTag', 'onRemoteMessageRequest exception');
            }
            return true;
        }
    }
    ```

3. USB接口初始化及参数设置。

    ```
    static napi_value GetProductDesc(napi_env env, napi_callback_info info) {
        ...... // 省略代码
        // 获取设备描述符
        auto [res, devDesc] = GetDeviceInfo();
        if (!res) {
            napi_get_undefined(env, &result);
            return result;
        }
    
        // 获取配置描述符，并占用接口
        if (!GetPipeInfo()) {
            napi_get_undefined(env, &result);
            return result;
        }
        // 初始化数据转换参数，vid及pid
        DataParser::GetInstance().Init(devDesc.idVendor, devDesc.idProduct);
    
        // 设置配置描述符
        SetFeature(devDesc.idProduct);
    
        // 获取产品描述信息
        auto productDesc = GetProductStringDescriptor(devDesc.iProduct);
        if (!productDesc.has_value()) {
            OH_LOG_ERROR(LOG_APP, "GetProductStringDescriptor failed");
            napi_get_undefined(env, &result);
            return result;
        }
        return CreateJsArray(env, productDesc.value(), devDesc.idProduct);
    }
    ```

    * GetPipeInfo：获取配置描述符，并占用接口

    ```
    static bool GetPipeInfo() {
        struct UsbDdkConfigDescriptor *config = nullptr;
        // 获取配置描述符
        auto ret = OH_Usb_GetConfigDescriptor(g_devHandle, 1, &config);
        if (ret != 0) {
            OH_LOG_ERROR(LOG_APP, "get config desc failed:%{public}d", ret);
            return false;
        }
        // 从配置描述符中找到手写板相关的接口和端点
        auto [res, interface, endpoint, maxPktSize] = GetInterfaceAndEndpoint(config);
        if (!res) {
            OH_LOG_ERROR(LOG_APP, "GetInterfaceAndEndpoint failed");
            return false;
        }
        // 释放配置描述符，防止内存泄露
        OH_Usb_FreeConfigDescriptor(config);
        g_dataEp = endpoint;
        g_maxPktSize = maxPktSize;
        g_interface = interface;
        // 占用接口，同时也会卸载内核键盘驱动
        ret = OH_Usb_ClaimInterface(g_devHandle, g_interface, &g_interfaceHandle);
        if (ret != 0) {
            OH_LOG_ERROR(LOG_APP, "claim interface failed%{public}d", ret);
            return false;
        }
        return true;
    }
    ```

    * SetFeature：向控制节点写入控制信息

    ```
    // 向设备写入配置信息
    static void SetFeature(uint16_t iProduct) {
        OH_LOG_INFO(LOG_APP, "SetFeature enter\n");

        if(iProduct != 0xA150 && iProduct != 0xA153) {
            OH_LOG_WARN(LOG_APP, "no need set feature\n");
            return;
        }

        // 设置feature
        uint32_t len = 100;
        struct UsbControlRequestSetup strDescSetup;
        strDescSetup.bmRequestType = 0x21;
        strDescSetup.bRequest = 0x09;
        strDescSetup.wValue = 0x03 << 8 | 0x02; // desc Index
        strDescSetup.wIndex = 0x0;
        strDescSetup.wLength = 0x02;
        uint8_t data[128] = {0x02, 0x02};
        uint32_t dataLen = 2;
        int32_t ret = OH_Usb_SendControlWriteRequest(g_interfaceHandle, &strDescSetup, UINT_MAX, data, dataLen);
        if(ret < 0) {
            OH_LOG_ERROR(LOG_APP, "OH_Usb_SendControlWriteRequest error ret:%{public}d", ret);
        }
        OH_LOG_INFO(LOG_APP, "SetFeature OK!");
    }
    ```

    * GetProductStringDescriptor：获取产品信息

    ```
    static std::optional<std::string> GetProductStringDescriptor(uint16_t iProduct) {
        uint8_t strDesc[100] = {0};
        // 获取产品字符串描述符
        uint32_t len = 100;
        struct UsbControlRequestSetup strDescSetup;
        strDescSetup.bmRequestType = 0x80;
        strDescSetup.bRequest = 0x06;
        strDescSetup.wValue = (0x03 << 8) | (iProduct); // desc Index
        strDescSetup.wIndex = 0x409;                    // language Id
        strDescSetup.wLength = len;
        auto ret = OH_Usb_SendControlReadRequest(g_interfaceHandle, &strDescSetup, UINT32_MAX, strDesc, &len);
        if (ret != 0) {
            OH_LOG_ERROR(LOG_APP, "send ctrl read failed%{public}d", ret);
            return {};
        }

        // 将unicode形式的描述符转化为ASCII, 便于打印
        std::string desc = UnicodeToAsc(strDesc + 2, len - 2);
        OH_LOG_INFO(LOG_APP, "strDesc %{public}s\n", desc.data());
        return desc;
    }
    ```

4. getBoardInput：创建异步任务，从设备获取输入信息。

    ```
    static napi_value GetBoardInput(napi_env env, napi_callback_info info) {
        ...... // 省略代码
        // 创建异步任务
        napi_status status = napi_create_async_work(env, nullptr, resource, g_getKeyboardExecute, g_getKeyboardComplete,
                                                    reinterpret_cast<void *>(asyncContext), &asyncContext->work);
        if (status != napi_ok) {
            OH_LOG_ERROR(LOG_APP, "%{public}s create async work failed", __func__);
            return result;
        }
        napi_queue_async_work(env, asyncContext->work);
        return result;
    }
    ```

    * g_getKeyboardExecute：执行任务

    ```
    static auto g_getKeyboardExecute = [](napi_env env, void *data) {
        AsyncContext *asyncContext = reinterpret_cast<AsyncContext *>(data);
        uint32_t bufferLen = g_maxPktSize;
        // 占用接口，同时也会卸载内核键盘驱动
        // 创建用于存放数据的缓冲区
        int32_t ret = OH_Usb_CreateDeviceMemMap(g_devHandle, bufferLen, &devMmap);
        if (ret != 0 || devMmap == nullptr) {
            return;
        }
        
        DataParser &parser = DataParser::GetInstance();
        parser.StartWork();
        stopIo = false;
        while (!stopIo) {
            // 读取手写板数据
            ret = GetUsbBoardInput(devMmap);
            if (ret != 0) {
                std::this_thread::sleep_for(std::chrono::seconds(1));
                continue;
            }
            // 处理读取到的数据
            parser.ParseData(devMmap->address, devMmap->transferedLength);
        }

        if (ret == 0) {
            asyncContext->status = napi_ok;
        } else {
            asyncContext->status = napi_generic_failure;
        }
    };
    ```

    * GetUsbBoardInput：通过中断方式读取设备输入信息

    ```
    static int32_t GetUsbBoardInput(UsbDeviceMemMap *devMmap) {
        struct UsbRequestPipe pipe;
        pipe.interfaceHandle = g_interfaceHandle;
        pipe.endpoint = g_dataEp;
        pipe.timeout = UINT32_MAX; //  等待5s

        // 通过USB中断传输方式，读取键值
        int32_t ret = OH_Usb_SendPipeRequest(&pipe, devMmap);
        if (ret != 0) {
            OH_LOG_ERROR(LOG_APP, "pipe read failed%{public}d", ret);
            return -1;
        }
        return 0;
    }
    ```

    * g_getKeyboardComplete：线程结束，进行释放资源

    ```
    static auto g_getKeyboardComplete = [](napi_env env, napi_status status, void *data) {
        AsyncContext *asyncContext = reinterpret_cast<AsyncContext *>(data);
        if (asyncContext->deferred) {
            napi_value result = nullptr;
            napi_create_int32(env, 0, &result);
            if (asyncContext->status == napi_ok) {
                napi_resolve_deferred(env, asyncContext->deferred, result);
            } else {
                napi_reject_deferred(env, asyncContext->deferred, result);
            }
        }

        napi_delete_async_work(env, asyncContext->work);
        delete asyncContext;
    };
    ```

5. 通过DataParser转换数据，向系统写入数据。

    * 创建键盘设备

    ```
    int32_t InjectThread::CreateKeyBoardDevice(std::string deviceName) const {
        // 设备属性
        Hid_Device hidDevice = {
            .deviceName = deviceName.c_str(), .vendorId = 0x6006, .productId = 0x6008, .version = 1, .bustype = BUS_USB};
        // 设备关注事件类型
        std::vector<Hid_EventType> eventType = {HID_EV_KEY};
        Hid_EventTypeArray eventTypeArray = {.hidEventType = eventType.data(), .length = (uint16_t)eventType.size()};
        // 设置设备关注的键值
        std::vector<Hid_KeyCode> keyCode = {
            HID_KEY_1,     HID_KEY_SPACE,      HID_KEY_BACKSPACE,   HID_KEY_ENTER,       HID_KEY_ESC,
            HID_KEY_SYSRQ, HID_KEY_LEFT_SHIFT, HID_KEY_RIGHT_SHIFT, HID_KEY_VOLUME_DOWN, HID_KEY_VOLUME_UP,
            HID_KEY_0,     HID_KEY_2,          HID_KEY_3,           HID_KEY_4,           HID_KEY_5,
            HID_KEY_6,     HID_KEY_7,          HID_KEY_8,           HID_KEY_9,           HID_KEY_A,
            HID_KEY_B,     HID_KEY_C,          HID_KEY_D,           HID_KEY_E,           HID_KEY_F,
            HID_KEY_G,     HID_KEY_H,          HID_KEY_I,           HID_KEY_J,           HID_KEY_K,
            HID_KEY_L,     HID_KEY_M,          HID_KEY_N,           HID_KEY_O,           HID_KEY_P,
            HID_KEY_Q,     HID_KEY_R,          HID_KEY_S,           HID_KEY_T,           HID_KEY_U,
            HID_KEY_V,     HID_KEY_W,          HID_KEY_X,           HID_KEY_Y,           HID_KEY_Z, HID_KEY_DELETE};
        Hid_KeyCodeArray keyCodeArray = {.hidKeyCode = keyCode.data(), .length = (uint16_t)keyCode.size()};
        // 设备关注的事件
        Hid_EventProperties hidEventProp = {.hidEventTypes = eventTypeArray, .hidKeys = keyCodeArray};
        // 创建以上属性的设备
        return CreateDevice(&hidDevice, &hidEventProp);
    }

    ```

    * 创建触摸设备

    ```
    int32_t InjectThread::CreateTouchPadDevice(std::string deviceName) const {
        std::vector<Hid_DeviceProp> deviceProp = {HID_PROP_DIRECT};
        Hid_Device hidDevice = {
            .deviceName = deviceName.c_str(), 
            .vendorId = 0x6006, 
            .productId = 0x6006, 
            .version = 1, 
            .bustype = BUS_USB,
            .properties = deviceProp.data(),
            .propLength = (uint16_t)deviceProp.size()
        };
        // 设备关注事件类型
        std::vector<Hid_EventType> eventType = {HID_EV_ABS, HID_EV_KEY, HID_EV_SYN, HID_EV_MSC};
        Hid_EventTypeArray eventTypeArray = {.hidEventType = eventType.data(), .length = (uint16_t)eventType.size()};
        // 设备关注按键键值范围
        std::vector<Hid_KeyCode> keyCode = {HID_BTN_TOOL_PEN, HID_BTN_TOOL_RUBBER, HID_BTN_TOUCH, HID_BTN_STYLUS, HID_BTN_RIGHT};
        Hid_KeyCodeArray keyCodeArray = {.hidKeyCode = keyCode.data(), .length = (uint16_t)keyCode.size()};
        // 设备关注特殊事件扫描
        std::vector<Hid_MscEvent> mscEvent = {HID_MSC_SCAN};
        Hid_MscEventArray mscEventArray = {.hidMscEvent = mscEvent.data(), .length = (uint16_t)mscEvent.size()};
        // 设备关注绝对事件坐标值及压力值
        std::vector<Hid_AbsAxes> absAxes = {HID_ABS_X, HID_ABS_Y, HID_ABS_PRESSURE};
        Hid_AbsAxesArray absAxesArray = {.hidAbsAxes = absAxes.data(), .length = (uint16_t)absAxes.size()};
        Hid_EventProperties hidEventProp = { 
            .hidEventTypes = eventTypeArray, 
            .hidKeys = keyCodeArray, 
            .hidAbs = absAxesArray,
            .hidMiscellaneous=mscEventArray
        };
        int32_t maxX, maxY, maxPressure;
        DataParser::GetInstance().GetAbsMax(maxX, maxY, maxPressure);
        OH_LOG_INFO(LOG_APP, "OH_Usb_CreateDevice maxX:%{public}d, maxY:%{public}d, maxPressure:%{public}d", maxX, maxY,
                    maxPressure);
        hidEventProp.hidAbsMin[HID_ABS_X] = 0;
        hidEventProp.hidAbsMin[HID_ABS_Y] = 0;
        hidEventProp.hidAbsMin[HID_ABS_PRESSURE] = 0;
        hidEventProp.hidAbsMax[HID_ABS_X] = maxX;
        hidEventProp.hidAbsMax[HID_ABS_Y] = maxY;
        hidEventProp.hidAbsMax[HID_ABS_PRESSURE] = maxPressure;
        // 创建以上属性的设备
        return CreateDevice(&hidDevice, &hidEventProp);
    }
    ```

## 项目运行

1.  安装开发工具[DevEco Studio](https://developer.huawei.com/consumer/cn/deveco-studio/)。
2.  下载该项目源码handwriting-board。
3.  将获取的源码导入DevEco Studio。

    a. 菜单栏选择“File->Open”，选择项目所在的文件夹。

    b. 信任项目，并配置SDK。

    c. 菜单栏选择“File->Setting->OpenHarmony SDK->Location”选择一个目录去存放SDK（只有初次安装时需要配置）。

    d. 菜单栏选择“Build->Clean Project”。
    
    e. 菜单栏选择“Build->Rebuild Project”。

4.  在项目的`entry\build\default\outputs\default`目录下就可以找到未签名的hap。
5.  签名hap后就可以运行在OpenHarmony/HarmonyOS系统上。
