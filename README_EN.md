# handwriting-board

#### Introduce
This project is a XX handwriting board proofing demo, this project is a handwriting tablet application, handwriting version driver integration project, it is recommended that the handwriting tablet application and the handwriting board driver separate project management is clearer。  
The main features of the tablet app：
- Device connection status display (query, binding device driver)
- Tablet button settings
- Tablet locale
- Exit (unbind the device driver)

Key features of the tablet driver app:
- USB communication with the device (get device input)
- Communicate with the system (write events to the system)

#### Software architecture
![Driver architecture diagram](./architecture_en.png)  
- Extended peripheral management service: mainly responsible for device management and driver package management
- Extended peripheral applications: The main responsibility is to customize the driver, including buttons, regions, etc
- Peripheral Extension Driver (Application): The main responsibility is to communicate with the system and equipment, and reflect the user's operation to the system

#### Explanation of key code
Write tablet to apply key code：  
1. In Index.ets, RpcTool is initialized on page load
```
aboutToAppear() {
    RpcTool.getInstance().init(this)
    RpcTool.getInstance().bindDevice()
}
```
2. Query the device list in RpcTool.ets and match the devices based on vorderId, productId   
import package：
```
import deviceManager from '@ohos.driver.deviceManager';

```
Query the list of devices：
```
  private query() {
    hilog.info(0, 'testTag ui', 'query enter');
    if (this.isBinding || this.bindState) {
      hilog.info(0, 'testTag ui', 'has bind. device id:' + this.deviceId)
      return
    }
    if (this.deviceId != -1) {
      hilog.info(0, 'testTag ui', 'has query. device id:' + this.deviceId)
      return;
    }
    this.isQuerying = true
    try {
      // Use the ArkTs API to query the list of USB devices
      let devices = deviceManager.queryDevices(deviceManager.BusType.USB);
      for (let item of devices) {
        let device = item as deviceManager.USBDevice;
        hilog.info(0, 'testTag ui', 'querydevice id:' + device.deviceId + ',bustype:' + device.busType
        + ',vid:' + device.vendorId + ',pid:' + device.productId + ', des:' + device.description);
        // Filter target devices by product ID
        let index = PRODUCT_ID_LIST.indexOf(device.productId)
        if (index >= 0) {
          this.deviceId = device.deviceId;
          this.vendorId = device.vendorId;
          this.productId = device.productId;
          this.description = device.description;
          break;
        }
      }
    } catch (error) {
      hilog.error(0, 'testTag ui', `Failed to query device. Code is ${error.code}, message is ${error.message}`);
    }
    this.message = this.deviceId.toString();
    this.isQuerying = false
  }
```
3. Bind the device to RpcTool.ets
```
  private async bind() {
    try {
      hilog.info(0, 'testTag ui', 'bindDevice id is:' + this.deviceId);
      // You can bind a device to a device driver through the ArkTs interface and use the device ID to obtain the remote object of the device driver
      let data = await deviceManager.bindDevice(this.deviceId, async (error, data) => {
        hilog.error(0, 'testTag ui', 'Device is disconnected:' + data);
        this.bindState = false;
        this.isBinding = false;
        try {
          await deviceManager.unbindDevice(data)
        } catch (e) {
          hilog.error(0, 'testTag ui', 'unbindDevice failed, failed code=' + e?.code);
        }
        this.bindStateChanged()
      });

      hilog.info(0, 'testTag ui', 'bindDevice success:' + data.deviceId);
      // Obtain the remote object of the device driver and communicate with the device driver through the remote object (configure driver parameters)
      this.remote = data.remote as rpc.RemoteObject;
      if (this.remote === null) {
        hilog.error(0, 'testTag ui', 'create remote fail');
        this.bindState = false;
        this.isBinding = false;
        return;
      }
      this.bindState = true;
      this.isBinding = false;
      hilog.info(0, 'testTag ui', 'create remote success');
    } catch (error) {
      hilog.error(0, 'testTag ui', 'bindDevice fail. Code:' + error + ', message:' + error.message);
      this.bindState = false;
      this.isBinding = false;
    }
  }

```
4. In RpcTool.ets, IPC communication with the driver is carried out according to the user's choice
```
  setKeyValue(keyIndex: number, keyValue: number) {
    // Directly call the RPC interface to send messages to the server, and the client needs to serialize the input parameters and deserialize the return value, which is cumbersome
    let option = new rpc.MessageOption();
    let dataSend = new rpc.MessageSequence();
    let reply = new rpc.MessageSequence();
    dataSend.writeInt(keyIndex);
    dataSend.writeInt(keyValue);
    hilog.info(0, 'testTag ui', `send key value to remote keyIndex:${keyIndex} keyValue:${keyValue}`);
    if (this.remote == null) {
      hilog.error(0, 'testTag ui', `the remote is null`)
      return;
    }
    // To communicate with the device driver through the driver's remote object, the developer needs to agree on the communication rules between the application side and the driver side
    this.remote.sendMessageRequest(REQUEST_CODE_KEY_VALUE, dataSend, reply, option).then((ret) => {
      let msg = reply.readInt();
      hilog.info(0, 'testTag ui', `setKeyValue ret:${ret} msg:${msg}`);
    }).catch((error) => {
      hilog.info(0, 'testTag ui', 'setKeyValue failed');
    });
  }

```

The tablet drives critical code：
1. Inherit the drive to extend the ability to DriverExtensionAbility，Onit to initialize the Usb interface
import package：
```
// Introduce and expand the capabilities of driver peripherals
import Extension from '@ohos.app.ability.DriverExtensionAbility';

```
Driver class：
```
export default class DriverExtAbility extends Extension {
    // Get the mapping of custom keys
    getKeyMapInfo() {
        let filePath = this.context.filesDir;
        let keyStorage = dataStorage.getStorageSync(filePath + '/keyInfo');
        STORAGE_KEY.forEach((value, index) => {
            defaultIndexMap[index] = keyStorage.getSync(value.key, value.default)
        })
    }

    // Get custom input directions for your tablet
    getDirectionInfo() {
        let filePath = this.context.filesDir;
        let keyStorage = dataStorage.getStorageSync(filePath + '/keyInfo');
        direction = keyStorage.getSync('direction', 0);
    }

    // When a device is plugged into the system, the extended peripheral framework will match the peripheral driver package with the pId through the vId, and after the matching, the extended peripheral driver will be pulled up through the system meta-capability, and onInit will be called when the meta-capability is pulled
    onInit(want): void {
        deviceId = want.parameters["deviceId"];
        // Initialize the USB port and obtain the description
        let ret = testNapi.getProductDesc(want.parameters["deviceId"]);
        if (ret != undefined) {
            [globalThis.productDesc, globalThis.productId] = ret;
        }
        this.getKeyMapInfo()
        this.getDirectionInfo()
        // Get the screen resolution
        display.getAllDisplays((err, data:Array<display.Display>) => {
            hilog.info(0, 'testTag', 'getAllDisplays callback');
            if (err.code) {
                testNapi.setDisplayInfo(2160, 1440);
            } else {
                testNapi.setDisplayInfo(data[0].width, data[0].height);
            }
            // Pass to the native layer
            testNapi.setButtonMapping(defaultIndexMap);
            testNapi.setDirection(direction);
            GetData();
            //PowerManagwer();
        });
        globalThis.context = this.context;

    }
    // Peripheral drive meta capabilities are released
    onRelease() :void {
        hilog.info(0, 'testTag', 'DriverAbility onRelease');
        // Release the underlying resources
        testNapi.releaseResource();
    }
    // Peripheral driver meta-capabilities are connected
    onConnect(want): rpc.RemoteObject {
        hilog.info(0, 'testTag', 'DriverAbility onConnect');
        globalThis?.connectStatus = true;
        let robj = new FirstDriverAbilityStub("remote")
        return robj;
    }
    // The peripheral drive element capability is disconnected
    onDisconnect(want): void {
        hilog.info(0, 'testTag', 'DriverAbility onDisconnect');
        globalThis?.connectStatus = false;
    }
    onDump(params): Array<string> {
        hilog.info(0, 'testTag', 'DriverAbility onDump, params:' + JSON.stringify(params));
        return ['params'];
    }
};

```
Apply the remote IPC stub class with the app：
```
// On the driver side, a remote object that communicates with the application side
class FirstDriverAbilityStub extends rpc.RemoteObject {
    constructor(des) {
        if (typeof des === 'string') {
            super(des);
        } else {
            return null;
        }
    }
    // The communication rules agreed with the application
    onRemoteMessageRequest(code: number,
        data: MessageSequence,
        reply: MessageSequence,
        options: MessageOption
    ) {
        hilog.info(0, 'testTag', `testtag driver extension onRemoteMessageRequest called ${code}`);
        try{
            // If the code is equal to 1, you can set a custom key mapping
            if (code == 1) {
                let keyIndex = data.readInt();
                let keyValue = data.readInt();
                console.info(`testtag driver extension start setOneButtonMapping key:${keyIndex} val:${keyValue}`);
                testNapi.setOneButtonMapping(keyIndex, keyValue);
                console.info(`testtag driver extension setOneButtonMapping called key:${keyIndex} val:${keyValue}`);
                reply.writeInt(keyIndex);
                return true;
            } else if (code == 2) {
                // code is equal to 2, which indicates that a custom direction is set
                let direction = data.readInt();
                console.info(`testtag driver start setDirection direction:${direction}`);
                testNapi.setDirection(direction);
                console.info(`testtag driver setDirection called`);
                reply.writeInt(direction);
                return true;
            } else if (code == 3) {
                // If the code is equal to 3, the connection status has changed
                let type = data.readInt();
                type = (globalThis?.connectStatus ? 1 : 0);
                hilog.info(0, 'testTag', `testtag get connection status called status:${type}`);
                reply.writeInt(type);
                return true;
            }
        } catch (error) {
            hilog.info(0, 'testTag', 'onRemoteMessageRequest exception');
        }
        return true;
    }
}

```
2. USB interface initialization and parameter setting
```
static napi_value GetProductDesc(napi_env env, napi_callback_info info) {
    ...... // Omit the code
    // Get the device descriptor
    auto [res, devDesc] = GetDeviceInfo();
    if (!res) {
        napi_get_undefined(env, &result);
        return result;
    }

    // Gets the configuration descriptor, and occupies the interface
    if (!GetPipeInfo()) {
        napi_get_undefined(env, &result);
        return result;
    }
    // Initialize data conversion parameters, vid, and pid
    DataParser::GetInstance().Init(devDesc.idVendor, devDesc.idProduct);

    // Set the configuration descriptor
    SetFeature(devDesc.idProduct);

    // Get product description information
    auto productDesc = GetProductStringDescriptor(devDesc.iProduct);
    if (!productDesc.has_value()) {
        OH_LOG_ERROR(LOG_APP, "GetProductStringDescriptor failed");
        napi_get_undefined(env, &result);
        return result;
    }
    return CreateJsArray(env, productDesc.value(), devDesc.idProduct);
}

```
GetPipeInfo：Gets the configuration descriptor, and occupies the interface
```
static bool GetPipeInfo() {
    struct UsbDdkConfigDescriptor *config = nullptr;
    // Get the configuration descriptor
    auto ret = OH_Usb_GetConfigDescriptor(g_devHandle, 1, &config);
    if (ret != 0) {
        OH_LOG_ERROR(LOG_APP, "get config desc failed:%{public}d", ret);
        return false;
    }
    // Locate the interfaces and endpoints related to the tablet from the configuration descriptor
    auto [res, interface, endpoint, maxPktSize] = GetInterfaceAndEndpoint(config);
    if (!res) {
        OH_LOG_ERROR(LOG_APP, "GetInterfaceAndEndpoint failed");
        return false;
    }
    // Release the configuration descriptor to prevent memory leaks
    OH_Usb_FreeConfigDescriptor(config);
    g_dataEp = endpoint;
    g_maxPktSize = maxPktSize;
    g_interface = interface;
    // occupies the interface, and also uninstalls the kernel keyboard driver
    ret = OH_Usb_ClaimInterface(g_devHandle, g_interface, &g_interfaceHandle);
    if (ret != 0) {
        OH_LOG_ERROR(LOG_APP, "claim interface failed%{public}d", ret);
        return false;
    }
    return true;
}

```
SetFeature：Writes control information to the control node
```
// Write configuration information to the device
static void SetFeature(uint16_t iProduct) {
    OH_LOG_INFO(LOG_APP, "SetFeature enter\n");

    if(iProduct != 0xA150 && iProduct != 0xA153) {
        OH_LOG_WARN(LOG_APP, "no need set feature\n");
        return;
    }

    // Set the feature
    uint32_t len = 100;
    struct UsbControlRequestSetup strDescSetup;
    strDescSetup.bmRequestType = 0x21;
    strDescSetup.bRequest = 0x09;
    strDescSetup.wValue = 0x03 << 8 | 0x02; // desc Index
    strDescSetup.wIndex = 0x0;
    strDescSetup.wLength = 0x02;
    uint8_t data[128] = {0x02, 0x02};
    uint32_t dataLen = 2;
    int32_t ret = OH_Usb_SendControlWriteRequest(g_interfaceHandle, &strDescSetup, UINT_MAX, data, dataLen);
    if(ret < 0) {
        OH_LOG_ERROR(LOG_APP, "OH_Usb_SendControlWriteRequest error ret:%{public}d", ret);
    }
    OH_LOG_INFO(LOG_APP, "SetFeature OK!");
}

```  
GetProductStringDescriptor：Get product information
```
static std::optional<std::string> GetProductStringDescriptor(uint16_t iProduct) {
    uint8_t strDesc[100] = {0};
    // Get the product string descriptor
    uint32_t len = 100;
    struct UsbControlRequestSetup strDescSetup;
    strDescSetup.bmRequestType = 0x80;
    strDescSetup.bRequest = 0x06;
    strDescSetup.wValue = (0x03 << 8) | (iProduct); // desc Index
    strDescSetup.wIndex = 0x409;                    // language Id
    strDescSetup.wLength = len;
    auto ret = OH_Usb_SendControlReadRequest(g_interfaceHandle, &strDescSetup, UINT32_MAX, strDesc, &len);
    if (ret != 0) {
        OH_LOG_ERROR(LOG_APP, "send ctrl read failed%{public}d", ret);
        return {};
    }

    // Convert unicode-like descriptors to ASCII for easy printing
    std::string desc = UnicodeToAsc(strDesc + 2, len - 2);
    OH_LOG_INFO(LOG_APP, "strDesc %{public}s\n", desc.data());
    return desc;
}

```
3. getBoardInput: Create an asynchronous task to get input information from the device
```
static napi_value GetBoardInput(napi_env env, napi_callback_info info) {
    ...... // Omit the code
    // Create an asynchronous task
    napi_status status = napi_create_async_work(env, nullptr, resource, g_getKeyboardExecute, g_getKeyboardComplete,
                                                reinterpret_cast<void *>(asyncContext), &asyncContext->work);
    if (status != napi_ok) {
        OH_LOG_ERROR(LOG_APP, "%{public}s create async work failed", __func__);
        return result;
    }
    napi_queue_async_work(env, asyncContext->work);
    return result;
}

```
g_getKeyboardExecute：Perform tasks
```
static auto g_getKeyboardExecute = [](napi_env env, void *data) {
    AsyncContext *asyncContext = reinterpret_cast<AsyncContext *>(data);
    uint32_t bufferLen = g_maxPktSize;
    // occupies the interface, and also uninstalls the kernel keyboard driver
    // Create a buffer for storing data
    int32_t ret = OH_Usb_CreateDeviceMemMap(g_devHandle, bufferLen, &devMmap);
    if (ret != 0 || devMmap == nullptr) {
        return;
    }
    
    DataParser &parser = DataParser::GetInstance();
    parser.StartWork();
    stopIo = false;
    while (!stopIo) {
        // Read tablet data
        ret = GetUsbBoardInput(devMmap);
        if (ret != 0) {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            continue;
        }
        // Process the read data
        parser.ParseData(devMmap->address, devMmap->transferedLength);
    }

    if (ret == 0) {
        asyncContext->status = napi_ok;
    } else {
        asyncContext->status = napi_generic_failure;
    }
};

```
GetUsbBoardInput：The device input information is read through the terminal
```
static int32_t GetUsbBoardInput(UsbDeviceMemMap *devMmap) {
    struct UsbRequestPipe pipe;
    pipe.interfaceHandle = g_interfaceHandle;
    pipe.endpoint = g_dataEp;
    pipe.timeout = UINT32_MAX; //  Wait 5s

    // The key value is read by USB interrupt transmission mode
    int32_t ret = OH_Usb_SendPipeRequest(&pipe, devMmap);
    if (ret != 0) {
        OH_LOG_ERROR(LOG_APP, "pipe read failed%{public}d", ret);
        return -1;
    }
    return 0;
}

```
g_getKeyboardComplete：When the thread ends, the resources are released

4. You can use DataParser to convert data and write data to the system
Create a keyboard device：
```
int32_t InjectThread::CreateKeyBoardDevice(std::string deviceName) const {
    // Device properties
    Hid_Device hidDevice = {
        .deviceName = deviceName.c_str(), .vendorId = 0x6006, .productId = 0x6008, .version = 1, .bustype = BUS_USB};
    // The type of event that the device focuses on
    std::vector<Hid_EventType> eventType = {HID_EV_KEY};
    Hid_EventTypeArray eventTypeArray = {.hidEventType = eventType.data(), .length = (uint16_t)eventType.size()};
    // Set the key value that the device cares about
    std::vector<Hid_KeyCode> keyCode = {
        HID_KEY_1,     HID_KEY_SPACE,      HID_KEY_BACKSPACE,   HID_KEY_ENTER,       HID_KEY_ESC,
        HID_KEY_SYSRQ, HID_KEY_LEFT_SHIFT, HID_KEY_RIGHT_SHIFT, HID_KEY_VOLUME_DOWN, HID_KEY_VOLUME_UP,
        HID_KEY_0,     HID_KEY_2,          HID_KEY_3,           HID_KEY_4,           HID_KEY_5,
        HID_KEY_6,     HID_KEY_7,          HID_KEY_8,           HID_KEY_9,           HID_KEY_A,
        HID_KEY_B,     HID_KEY_C,          HID_KEY_D,           HID_KEY_E,           HID_KEY_F,
        HID_KEY_G,     HID_KEY_H,          HID_KEY_I,           HID_KEY_J,           HID_KEY_K,
        HID_KEY_L,     HID_KEY_M,          HID_KEY_N,           HID_KEY_O,           HID_KEY_P,
        HID_KEY_Q,     HID_KEY_R,          HID_KEY_S,           HID_KEY_T,           HID_KEY_U,
        HID_KEY_V,     HID_KEY_W,          HID_KEY_X,           HID_KEY_Y,           HID_KEY_Z, HID_KEY_DELETE};
    Hid_KeyCodeArray keyCodeArray = {.hidKeyCode = keyCode.data(), .length = (uint16_t)keyCode.size()};
    // Events that the device is concerned about
    Hid_EventProperties hidEventProp = {.hidEventTypes = eventTypeArray, .hidKeys = keyCodeArray};
    // Create a device with the above attributes
    return CreateDevice(&hidDevice, &hidEventProp);
}

```
Create a touch device：
```
int32_t InjectThread::CreateTouchPadDevice(std::string deviceName) const {
    std::vector<Hid_DeviceProp> deviceProp = {HID_PROP_DIRECT};
    Hid_Device hidDevice = {
        .deviceName = deviceName.c_str(), 
        .vendorId = 0x6006, 
        .productId = 0x6006, 
        .version = 1, 
        .bustype = BUS_USB,
        .properties = deviceProp.data(),
        .propLength = (uint16_t)deviceProp.size()
    };
    // The type of event that the device focuses on
    std::vector<Hid_EventType> eventType = {HID_EV_ABS, HID_EV_KEY, HID_EV_SYN, HID_EV_MSC};
    Hid_EventTypeArray eventTypeArray = {.hidEventType = eventType.data(), .length = (uint16_t)eventType.size()};
    // The device is concerned about the range of key values
    std::vector<Hid_KeyCode> keyCode = {HID_BTN_TOOL_PEN, HID_BTN_TOOL_RUBBER, HID_BTN_TOUCH, HID_BTN_STYLUS, HID_BTN_RIGHT};
    Hid_KeyCodeArray keyCodeArray = {.hidKeyCode = keyCode.data(), .length = (uint16_t)keyCode.size()};
    // The device is concerned about special event scanning
    std::vector<Hid_MscEvent> mscEvent = {HID_MSC_SCAN};
    Hid_MscEventArray mscEventArray = {.hidMscEvent = mscEvent.data(), .length = (uint16_t)mscEvent.size()};
    // The device pays attention to the absolute event coordinate value and the pressure value
    std::vector<Hid_AbsAxes> absAxes = {HID_ABS_X, HID_ABS_Y, HID_ABS_PRESSURE};
    Hid_AbsAxesArray absAxesArray = {.hidAbsAxes = absAxes.data(), .length = (uint16_t)absAxes.size()};
    Hid_EventProperties hidEventProp = { 
        .hidEventTypes = eventTypeArray, 
        .hidKeys = keyCodeArray, 
        .hidAbs = absAxesArray,
        .hidMiscellaneous=mscEventArray
    };
    int32_t maxX, maxY, maxPressure;
    DataParser::GetInstance().GetAbsMax(maxX, maxY, maxPressure);
    OH_LOG_INFO(LOG_APP, "OH_Usb_CreateDevice maxX:%{public}d, maxY:%{public}d, maxPressure:%{public}d", maxX, maxY,
                maxPressure);
    hidEventProp.hidAbsMin[HID_ABS_X] = 0;
    hidEventProp.hidAbsMin[HID_ABS_Y] = 0;
    hidEventProp.hidAbsMin[HID_ABS_PRESSURE] = 0;
    hidEventProp.hidAbsMax[HID_ABS_X] = maxX;
    hidEventProp.hidAbsMax[HID_ABS_Y] = maxY;
    hidEventProp.hidAbsMax[HID_ABS_PRESSURE] = maxPressure;
    // Create a device with the above attributes
    return CreateDevice(&hidDevice, &hidEventProp);
}

```
