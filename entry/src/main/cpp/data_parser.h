//
// Created on 2023/7/5.
//
// Node APIs are not fully supported. To solve the compilation error of the interface cannot be found,
// please include "napi/native_api.h".

#ifndef DATA_PARSER_H
#define DATA_PARSER_H

#include "hid_simulator.h"
#include "parse_point_info.h"

#define MAX_KEY_NUM 4

class DataParser {
  public:
    DataParser() = default;

    virtual ~DataParser() = default;

    static DataParser &GetInstance();
    
    void Init(int vid, int pid);

    // 解析数据
    virtual int32_t ParseData(const uint8_t *buffer, uint32_t length);

    virtual bool IsPenUp(const uint8_t *buffer, uint32_t length);

    virtual bool IsPenMove(const uint8_t *buffer, uint32_t length);

    // 设置keycode map
    void UpdateKeyCodeMap(int keyIndex, int keyValue);

    // 设置屏幕宽高
    void SetScreenSize(int screenWidth, int screenHeight);
    
    void GetAbsMax(int32_t &absXMax, int32_t &absYMax, int32_t &absPressureMax);
    
    void SetDirection(uint32_t direction);
    
    uint32_t GetDirection();

    // 开启EmitEvent
    void StartWork();

  private:
    int32_t ParseKeyData(const uint8_t *buffer, uint32_t length);

    int32_t ParseMoveData(const uint8_t *buffer, uint32_t length);

    int32_t ParsePenKeyData(const uint8_t *buffer, uint32_t length);

    int32_t ParseEventData(const uint8_t *buffer, uint32_t length);

    int32_t ParsePrivateData(const uint8_t *buffer, uint32_t length);

    void ConvertAbs(uint32_t &absX, uint32_t &absY);
    
  private:
    int screenWidth_;
    int screenHeight_;
    EventData eventData_;
    EventData lastData_;
    int keyCodeMap_[MAX_KEY_NUM];
    HidSimulator simulator_;
};

#endif // DATA_PARSER_H
