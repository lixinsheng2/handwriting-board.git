//
// Created on 2023/7/5.
//
// Node APIs are not fully supported. To solve the compilation error of the interface cannot be found,
// please include "napi/native_api.h".

#define LOG_TAG "testTag [NATIVE]"

#include "data_parser.h"
#include <hilog/log.h>

#define MIN_BUFFER_SIZE 10

#define ABSX_MAX_A13F (0x5750)
#define ABSY_MAX_A13F (0x3692)

#define ABSX_MAX_A153 (0x6338)
#define ABSY_MAX_A153 (0x6338)

static int g_absXMax = 1;
static int g_absYMax = 1;

static bool g_isKeyDown = false;
static bool g_isPenDown = false;
static bool g_isRightKeyDown = false;
static int g_direct = 1;
static int g_keyIndex = -1;

#define HANDWRITING_ABSPRESSURE_MAX 8191

char outStr[256] = {0};
static void PrintBuffer(const uint8_t *buffer, uint32_t length) {
    int32_t index = 0;
    for (uint32_t idx = 0; idx < length; ++idx) {
        index += sprintf(outStr + index, "%02X ", buffer[idx]);
    }
    OH_LOG_DEBUG(LOG_APP, "recv data len: %{public}u : %{public}s", length, outStr);
}

static void Reset() {
    g_isKeyDown = false;
    g_isPenDown = false;
}

// 单例
DataParser &DataParser::GetInstance() {
    static DataParser parser;
    return parser;
}

void DataParser::Init(int vid, int pid) {
    if (pid == 0xA13F) {
        g_absXMax = ABSX_MAX_A13F;
        g_absYMax = ABSY_MAX_A13F;
    } else if (pid == 0xA150 || pid == 0xA153) {
        g_absXMax = ABSX_MAX_A153;
        g_absYMax = ABSY_MAX_A153;
    } else {
        OH_LOG_ERROR(LOG_APP, "invalid vid:%{public}x & pid:%{public}x", vid, pid);
    }
    g_isKeyDown = false;
    g_isPenDown = false;
    g_isRightKeyDown = false;
    g_keyIndex = -1;
}

void DataParser::StartWork() {
    simulator_.StartWork();
}

// 更新按键映射键值
void DataParser::UpdateKeyCodeMap(int keyIndex, int keyValue) {
    keyCodeMap_[keyIndex] = keyValue;
}

void DataParser::SetScreenSize(int screenWidth, int screenHeight) {
    screenWidth_ = screenWidth;
    screenHeight_ = screenHeight;
}

void DataParser::GetAbsMax(int32_t &absXMax, int32_t &absYMax, int32_t &absPressureMax) {
    if (screenWidth_ > screenHeight_) {
        absXMax = g_absXMax;
        absYMax = g_absYMax;
    } else {
        absXMax = g_absYMax;
        absYMax = g_absXMax;
    }
    absPressureMax = HANDWRITING_ABSPRESSURE_MAX;
}

// 根据屏幕宽高，调整手写板方向
void DataParser::ConvertAbs(uint32_t &absX, uint32_t &absY) {
    float temp = 0;
    switch(g_direct) {
        case 1: // 手写板 90度
            temp = absX;
            absX = ((float)absY / g_absYMax * g_absXMax);
            absY = ((float)(g_absXMax - temp) / g_absXMax * g_absYMax);
            break;
        case 2: // 手写板 180度
            absX = g_absXMax - absX;
            absY = g_absYMax - absY;
            break;
        case 3: // 手写板 270度
            temp = absY;
            absY = ((float)absX / g_absXMax * g_absYMax);
            absX = ((float)(g_absYMax - temp) / g_absYMax * g_absXMax);
        case 0: // 手写板 0度
        default:
            break;
    }
}

void DataParser::SetDirection(uint32_t direction) {
    g_direct = direction;
}

uint32_t DataParser::GetDirection() {
    return g_direct;
}

int32_t DataParser::ParseKeyData(const uint8_t *buffer, uint32_t length) {
    g_isKeyDown = true;
    if (buffer[3] == 0x1) {
        g_keyIndex = 0;
    } else if (buffer[3] == 0x2) {
        g_keyIndex = 1;
    } else if (buffer[3] == 0x4) {
        g_keyIndex = 2;
    } else if (buffer[3] == 0x8) {
        g_keyIndex = 3;
    } else {
        g_isKeyDown = false;
        if (g_keyIndex == -1) {
            g_keyIndex = 0;
        }
    }
    eventData_.type = EVENT_TYPE_KEY;
    eventData_.changed = true;
    eventData_.isDown = g_isKeyDown;
    eventData_.keyVal = keyCodeMap_[g_keyIndex];
    return 0;
}

int32_t DataParser::ParsePenKeyData(const uint8_t *buffer, uint32_t length) {
    // 按下按键，或者抬起按键时，buffer[1] = 0，其他时刻是2
    if (buffer[1] == 0) {
        g_isRightKeyDown = !g_isRightKeyDown;
        eventData_.changed = true;
    }
    uint32_t xOffset = *((uint16_t *)&buffer[2]);
    uint32_t yOffset = *((uint16_t *)&buffer[4]);
    ConvertAbs(xOffset, yOffset);
    if (eventData_.changed) {
        eventData_.type = EVENT_TYPE_PEN_KEY;
        eventData_.isDown = g_isRightKeyDown;
    } else {
        eventData_.type = EVENT_TYPE_MOVE;
        eventData_.isDown = false;
    }
    eventData_.absX = xOffset;
    eventData_.absY = yOffset;
    return 0;
}


bool DataParser::IsPenUp(const uint8_t *buffer, uint32_t length) {
    // 笔是否接触到手写板，接触到则为1，不接触到则为0
    return (length < MIN_BUFFER_SIZE || buffer[1] != 0xA1);
}

bool DataParser::IsPenMove(const uint8_t *buffer, uint32_t length) {
    return (length >= MIN_BUFFER_SIZE && buffer[0] == 0x03);
}

int32_t DataParser::ParseMoveData(const uint8_t *buffer, uint32_t length) {
    // 笔是否接触到手写板，接触到则为1，不接触到则为0
    bool downState = (buffer[1] == 0xA1);
    if (g_isPenDown != downState) {
        eventData_.changed = true;
        g_isPenDown = downState;
    }

    // x offset
    uint32_t xOffset = *((uint16_t *)&buffer[2]);

    // y offset
    uint32_t yOffset = *((uint16_t *)&buffer[4]);

    // press
    uint16_t pressValue = *((uint16_t *)&buffer[6]);

    ConvertAbs(xOffset, yOffset);
    eventData_.type = EVENT_TYPE_MOVE;
    eventData_.absX = xOffset;
    eventData_.absY = yOffset;
    eventData_.isDown = g_isPenDown;
    eventData_.pressVal = pressValue;
    return 0;
}

int32_t DataParser::ParsePrivateData(const uint8_t *buffer, uint32_t length) {
    auto [withinRange, x, y, pressure, btnRight] = ParsePointInfo((uint8_t *)buffer, length);
    // 1 笔到达时清理现场
    if (!withinRange) {
        if(btnRight) {
            OH_LOG_INFO(LOG_APP, "Pen is comming");
            Reset();
        } else {
            OH_LOG_INFO(LOG_APP, "Pen is leave");
        }
        return -1;
    }
    OH_LOG_ERROR(LOG_APP, "withinRange:%{public}d btnRight:%{public}d g_isRightKeyDown:%{public}d",withinRange, btnRight, g_isRightKeyDown);
    // 如果笔上按键动作变化，则发送 EVENT_TYPE_PEN_KEY
    if (g_isRightKeyDown != btnRight) {
        eventData_.changed = true;
        g_isRightKeyDown = btnRight;
        eventData_.isDown = g_isRightKeyDown;
        eventData_.type = EVENT_TYPE_PEN_KEY;
    } else {
        bool downState = (pressure > 0);
        if (g_isPenDown != downState) {
            eventData_.changed = true;
            g_isPenDown = downState;
        }
        eventData_.isDown = g_isPenDown;
        eventData_.type = EVENT_TYPE_MOVE;
    }
    ConvertAbs(x, y);
    eventData_.absX = x;
    eventData_.absY = y;
    eventData_.pressVal = pressure;
    return 0;
}

int32_t DataParser::ParseData(const uint8_t *buffer, uint32_t length) {
    PrintBuffer(buffer, length);
    if (length < MIN_BUFFER_SIZE) {
        OH_LOG_ERROR(LOG_APP, "invalid length");
        return -1;
    }
    int ret = -1;
    if (buffer[0] == 0x03) { // 笔的移动或者按下事件
        ret = ParseMoveData(buffer, length);
    } else if (buffer[0] == 0x0c) { // 按键的按下或者抬起事件
        ret = ParseKeyData(buffer, length);
    } else if (buffer[0] == 0x1) { // 笔上的按键，无法区分上下键
        ret = ParsePenKeyData(buffer, length);
    } else if (buffer[0] == 0x2) { // 私有协议
        ret = ParsePrivateData(buffer, length);
    } else {
        OH_LOG_INFO(LOG_APP, "recv not interesting data");
    }
    OH_LOG_INFO(LOG_APP, "type:%{public}d isDown:%{public}d press:%{public}d"
                          " {x:%{public}d, y:%{public}d} key:%{public}d",
                 eventData_.type, eventData_.isDown, eventData_.pressVal,
                 eventData_.absX, eventData_.absY, eventData_.keyVal);
    if (ret == 0) {
        simulator_.ConvertEventData(eventData_);
    }
    lastData_ = eventData_;
    eventData_.Reset();
    return ret;
}
