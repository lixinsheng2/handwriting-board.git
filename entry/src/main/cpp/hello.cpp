#include <bits/alltypes.h>
#define LOG_TAG "testTag [NATIVE]"

#include "napi/native_api.h"
#include "usb/usb_ddk_api.h"
#include "hilog/log.h"

#include <fcntl.h>
#include <js_native_api.h>
#include <js_native_api_types.h>
#include <memory>
#include <tuple>
#include <usb/usb_ddk_types.h>
#include <string>
#include <optional>
#include "data_parser.h"
#include "key_code.h"
#include <functional>
#include <time.h>

#include "ddk/ddk_api.h"
#include <ddk/ddk_types.h>

uint64_t g_devHandle = 0;
uint64_t g_interfaceHandle = 0;
uint8_t g_dataEp = 0;
uint8_t g_interface = 0;
uint16_t g_maxPktSize = 0;
bool stopIo = false;
UsbDeviceMemMap *devMmap = nullptr;
DDK_Ashmem *ashmem = nullptr;
constexpr size_t BTN_MAPPING_INFO_SIZE = 4;
constexpr size_t PRODUCT_DESC_MAX_LEN = 100;
constexpr unsigned long long TIME_CONVERSION_NS_S = 1000000000ULL;

// 将unicode编码形式的字符串转化为ASCII码
std::string UnicodeToAsc(unsigned char *in, int inLen) {
    wchar_t wstr[PRODUCT_DESC_MAX_LEN] = {0};
    unsigned short tmpShort;
    int indexCnt = 0;

    for (uint32_t j = 0; j < inLen; j += 2) {
        memcpy(&tmpShort, in + j, 2);
        wstr[indexCnt++] = tmpShort;
    }
    char str[PRODUCT_DESC_MAX_LEN] = {0};
    wcstombs(str, wstr, PRODUCT_DESC_MAX_LEN);
    return std::string(str);
}

#define IS_INT_IN(epDesc) ((((epDesc.bEndpointAddress >> 7) & 1) == 1) && ((epDesc.bmAttributes & 0x03) == 0x03))
// 获取手写板的接口和端点
static std::tuple<bool, uint8_t, uint8_t, uint16_t> GetInterfaceAndEndpoint(const struct UsbDdkConfigDescriptor *config) {
    for (uint32_t intIdx = 0; intIdx < config->configDescriptor.bNumInterfaces; ++intIdx) {
        struct UsbDdkInterfaceDescriptor *intDesc = config->interface[intIdx].altsetting;
        uint32_t numSetting = config->interface[intIdx].numAltsetting;
        for (uint32_t setIdx = 0; setIdx < numSetting; ++setIdx) {
            uint32_t numEp = intDesc[setIdx].interfaceDescriptor.bNumEndpoints;
            struct UsbDdkEndpointDescriptor *epDesc = intDesc[setIdx].endPoint;
            for (uint32_t epIdx = 0; epIdx < numEp; ++epIdx) {
                // interrupt in
                if (IS_INT_IN((epDesc[epIdx].endpointDescriptor))) {
                    OH_LOG_INFO(LOG_APP, "bInterfaceNumber: %{public}u\n", intDesc[setIdx].interfaceDescriptor.bInterfaceNumber);
                    OH_LOG_INFO(LOG_APP, "bEndpointAddress: %{public}u\n", epDesc[epIdx].endpointDescriptor.bEndpointAddress);
                    return {true, intDesc[setIdx].interfaceDescriptor.bInterfaceNumber, epDesc[epIdx].endpointDescriptor.bEndpointAddress, epDesc[epIdx].endpointDescriptor.wMaxPacketSize};
                }
            }
        }
    }
    return {false, {}, {}, {}};
}

struct AsyncContext {
    napi_env env;
    napi_async_work work;

    napi_deferred deferred;
    napi_status status;
};
//static int32_t GetUsbBoardInput(DDK_Ashmem *ashmem ) {
static int32_t GetUsbBoardInput(UsbDeviceMemMap *devMmap) {
    struct UsbRequestPipe pipe;
    pipe.interfaceHandle = g_interfaceHandle;
    pipe.endpoint = g_dataEp;
    pipe.timeout = 5000; //  等待5s

    // 通过USB中断传输方式，读取键值
    int32_t ret = OH_Usb_SendPipeRequest(&pipe, devMmap);
    //int32_t ret = OH_Usb_SendPipeRequestWithAshmem(&pipe, ashmem);
    if (ret != 0) {
        OH_LOG_ERROR(LOG_APP, "pipe read failed%{public}d", ret);
        return -1;
    }
    return 0;
}

static auto g_getKeyboardExecute = [](napi_env env, void *data) {
    AsyncContext *asyncContext = reinterpret_cast<AsyncContext *>(data);
    uint32_t bufferLen = g_maxPktSize;
    // 占用接口，同时也会卸载内核键盘驱动
    // 创建用于存放数据的缓冲区
    int32_t ret = OH_Usb_CreateDeviceMemMap(g_devHandle, bufferLen, &devMmap);
    if (ret != 0 || devMmap == nullptr) {
        OH_LOG_ERROR(LOG_APP, "OH_Usb_CreateDeviceMemMap failed");
        asyncContext->status = napi_generic_failure;
        return;
    }
//     const uint8_t name[100] = "TestAshmem";
//     int32_t ret = OH_DDK_CreateAshmem(name, bufferLen, &ashmem);
//     if (ret != 0 || ashmem == nullptr) {
//         OH_LOG_ERROR(LOG_APP, "OH_DDK_CreateAshmem failed");
//         asyncContext->status = napi_generic_failure;
//         return;
//     }
//     OH_LOG_INFO(LOG_APP, "AAA OH_DDK_CreateAshmem, ashmemFd = %{public}d", ashmem->ashmemFd);
//     uint8_t ashmemMapType = 0x03;
//     ret = OH_DDK_MapAshmem(ashmem, ashmemMapType);
//     if (ret != 0 ) {
//         OH_LOG_ERROR(LOG_APP, "OH_DDK_MapAshmem failed");
//         asyncContext->status = napi_generic_failure;
//         return;
//     }
//     OH_LOG_INFO(LOG_APP, "AAA OH_DDK_MapAshmem\n");

    DataParser &parser = DataParser::GetInstance();
    parser.StartWork();
    OH_LOG_INFO(LOG_APP, "parser.StartWork");
    stopIo = false;

    struct timespec ts = {};
    uint64_t timestamp1 = 0, timestamp2 = 0;
    uint64_t timerSum = 0;
    uint64_t cout = 0;
    while (!stopIo) {

        struct UsbRequestPipe pipe;
        pipe.interfaceHandle = g_interfaceHandle;
        pipe.endpoint = g_dataEp;
        pipe.timeout = 5000; //  等待5s

        clock_gettime(CLOCK_MONOTONIC, &ts);
        timestamp1 = ts.tv_nsec + ts.tv_sec * TIME_CONVERSION_NS_S;
        // 读取手写板数据
        // 通过USB中断传输方式，读取键值
        int32_t ret = OH_Usb_SendPipeRequest(&pipe, devMmap);
//      int32_t ret = OH_Usb_SendPipeRequestWithAshmem(&pipe, ashmem);
        clock_gettime(CLOCK_MONOTONIC, &ts);
        timestamp2 = ts.tv_nsec + ts.tv_sec * TIME_CONVERSION_NS_S;
        if (ret != 0) {
            OH_LOG_ERROR(LOG_APP, "GetUsbKeyboardInput failed");
            std::this_thread::sleep_for(std::chrono::seconds(1));
            continue;
        }
        if (parser.IsPenMove(devMmap->address, devMmap->transferedLength) && parser.IsPenUp(devMmap->address, devMmap->transferedLength)) {
            if (timerSum > 0 && cout > 0) {
                OH_LOG_INFO(LOG_APP, "cout:%{public}lu, pipe read time AVG spent : %{public}lu ns", cout,
                            (timerSum / cout));
            }
            
            timerSum = 0;
            cout = 0;
        } else if (parser.IsPenMove(devMmap->address, devMmap->transferedLength) && !parser.IsPenUp(devMmap->address, devMmap->transferedLength)) {
            timerSum += (timestamp2 - timestamp1);
            cout++;
        }
//         if (parser.IsPenMove(ashmem->address, ashmem->transferredLength) && parser.IsPenUp(ashmem->address, ashmem->transferredLength)) {
//             if (timerSum > 0 && cout > 0) {
//                 OH_LOG_INFO(LOG_APP, "cout:%{public}llu, pipe read time AVG spent : %{public}llu ns", cout,
//                             (timerSum / cout));
//             }
//             timerSum = 0;
//             cout = 0;
//         } else if (parser.IsPenMove(ashmem->address, ashmem->transferredLength) && !parser.IsPenUp(ashmem->address, ashmem->transferredLength)) {
//             timerSum += (timestamp2 - timestamp1);
//             cout++;
//         }

//        OH_LOG_INFO(LOG_APP, "cout:%{public}lu, timestamp1: %{public}lu ns, isMove: %{public}d, isPenUp: %{public}d", cout, timestamp1, parser.IsPenMove(devMmap->address, devMmap->transferedLength), parser.IsPenUp(devMmap->address, devMmap->transferedLength));
//        if (!parser.IsPenMove(devMmap->address, devMmap->transferedLength) || parser.IsPenUp(devMmap->address, devMmap->transferedLength)) {
//            if (timestamp1 > 0 && cout > 0) {
//                clock_gettime(CLOCK_MONOTONIC, &ts);
//                uint64_t timestamp2 = ts.tv_nsec + ts.tv_sec * TIME_CONVERSION_NS_S;
//                OH_LOG_INFO(LOG_APP, "start penUp timer=%{public}lu, cout=%{public}lu", timestamp2);
//                OH_LOG_INFO(LOG_APP, "cout:%{public}lu, pipe read time AVG spent : %{public}lu ns", cout,
//                            ((timestamp2 - timestamp1) / cout));
//            }
//            timestamp1 = 0;
//            cout = 0;
//        } else {
//            cout ++;
//        }
        // 处理读取到的数据
        parser.ParseData(devMmap->address, devMmap->transferedLength);
    }
    OH_LOG_ERROR(LOG_APP, "stopIo end");
    if (ret == 0) {
        asyncContext->status = napi_ok;
    } else {
        asyncContext->status = napi_generic_failure;
    }
};

static auto g_getKeyboardComplete = [](napi_env env, napi_status status, void *data) {
    AsyncContext *asyncContext = reinterpret_cast<AsyncContext *>(data);
    if (asyncContext->deferred) {
        napi_value result = nullptr;
        napi_create_int32(env, 0, &result);
        if (asyncContext->status == napi_ok) {
            napi_resolve_deferred(env, asyncContext->deferred, result);
        } else {
            napi_reject_deferred(env, asyncContext->deferred, result);
        }
    }

    napi_delete_async_work(env, asyncContext->work);
    delete asyncContext;
};

static napi_value GetBoardInput(napi_env env, napi_callback_info info) {
    OH_LOG_INFO(LOG_APP, "enter GetBoardInput\n");

    auto asyncContext = new (std::nothrow) AsyncContext();
    asyncContext->env = env;
    asyncContext->status = napi_ok;

    napi_value result = nullptr;
    napi_create_promise(env, &asyncContext->deferred, &result);
    napi_value resource = nullptr;
    napi_create_string_utf8(env, "GetKbdInput", NAPI_AUTO_LENGTH, &resource);
    // 创建异步任务
    napi_status status = napi_create_async_work(env, nullptr, resource, g_getKeyboardExecute, g_getKeyboardComplete,
                                                reinterpret_cast<void *>(asyncContext), &asyncContext->work);
    if (status != napi_ok) {
        OH_LOG_ERROR(LOG_APP, "%{public}s create async work failed", __func__);
        return result;
    }
    napi_queue_async_work(env, asyncContext->work);
    return result;
}

static std::optional<std::string> GetProductStringDescriptor(uint16_t iProduct) {
    uint8_t strDesc[100] = {0};
    // 获取产品字符串描述符
    uint32_t len = 100;
    struct UsbControlRequestSetup strDescSetup;
    strDescSetup.bmRequestType = 0x80;
    strDescSetup.bRequest = 0x06;
    strDescSetup.wValue = (0x03 << 8) | (iProduct); // desc Index
    strDescSetup.wIndex = 0x409;                    // language Id
    strDescSetup.wLength = len;
    auto ret = OH_Usb_SendControlReadRequest(g_interfaceHandle, &strDescSetup, UINT32_MAX, strDesc, &len);
    if (ret != 0) {
        OH_LOG_ERROR(LOG_APP, "send ctrl read failed%{public}d", ret);
        return {};
    }

    // 将unicode形式的描述符转化为ASCII, 便于打印
    std::string desc = UnicodeToAsc(strDesc + 2, len - 2);
    OH_LOG_INFO(LOG_APP, "strDesc %{public}s\n", desc.data());
    return desc;
}

static napi_value CreateJsArray(napi_env env, const std::string &desc, uint16_t idProduct) {
    napi_value result;
    // 将获取到的产品描述符传给js层
    napi_value jsDesc;
    if (napi_create_string_utf8(env, desc.data(), desc.length(), &jsDesc) != napi_ok) {
        OH_LOG_ERROR(LOG_APP, "%{public}s napi_create_string_utf8 failed", __func__);
        napi_get_undefined(env, &result);
        return result;
    }
    napi_value jsProductId;
    if (napi_create_uint32(env, idProduct, &jsProductId) != napi_ok) {
        OH_LOG_ERROR(LOG_APP, "%{public}s napi_create_uint32 failed", __func__);
        napi_get_undefined(env, &result);
        return result;
    }
    napi_value jsArray;
    if (napi_create_array(env, &jsArray) != napi_ok) {
        OH_LOG_ERROR(LOG_APP, "%{public}s napi_create_array failed", __func__);
        napi_get_undefined(env, &result);
        return result;
    }
    if (napi_set_element(env, jsArray, 0, jsDesc) != napi_ok) {
        OH_LOG_ERROR(LOG_APP, "%{public}s napi_set_element 0 failed", __func__);
        napi_get_undefined(env, &result);
        return result;
    }
    if (napi_set_element(env, jsArray, 1, jsProductId) != napi_ok) {
        OH_LOG_ERROR(LOG_APP, "%{public}s napi_set_element 1 failed", __func__);
        napi_get_undefined(env, &result);
        return result;
    }
    return jsArray;
}

static bool GetPipeInfo() {
    OH_LOG_INFO(LOG_APP, "GetPipeInfo enter");
    struct UsbDdkConfigDescriptor *config = nullptr;
    // 获取配置描述符
    auto ret = OH_Usb_GetConfigDescriptor(g_devHandle, 1, &config);
    OH_LOG_INFO(LOG_APP, "OH_Usb_GetConfigDescriptor ret = %{public}d", ret);
    if (ret != 0) {
        OH_LOG_ERROR(LOG_APP, "get config desc failed:%{public}d", ret);
        return false;
    }
    // 从配置描述符中找到手写板相关的接口和端点
    auto [res, interface, endpoint, maxPktSize] = GetInterfaceAndEndpoint(config);
    OH_LOG_INFO(LOG_APP, "OH_Usb_GetConfigDescriptor ret = %{public}d", res);
    if (!res) {
        OH_LOG_ERROR(LOG_APP, "GetInterfaceAndEndpoint failed");
        return false;
    }
    // 释放配置描述符，防止内存泄露
    OH_Usb_FreeConfigDescriptor(config);
    g_dataEp = endpoint;
    g_maxPktSize = maxPktSize;
    g_interface = interface;
    // 占用接口，同时也会卸载内核键盘驱动
    ret = OH_Usb_ClaimInterface(g_devHandle, g_interface, &g_interfaceHandle);
//    OH_LOG_ERROR(LOG_APP, "claim interface g_interfaceHandle=%{public}llu", g_interfaceHandle);
    if (ret != 0) {
        OH_LOG_ERROR(LOG_APP, "claim interface failed%{public}d", ret);
        return false;
    }
    return true;
}

static std::tuple<bool, UsbDeviceDescriptor> GetDeviceInfo() {
//    OH_LOG_INFO(LOG_APP, "devHandle:%{public}llu\n", g_devHandle);
    // ddk 初始化
    int32_t ret = OH_Usb_Init();
    OH_LOG_INFO(LOG_APP, "OH_Usb_Init ret=:%{public}d\n", ret);
    if (ret != 0) {
        OH_LOG_ERROR(LOG_APP, "init ddk failed");
        return {false, {}};
    }

    struct UsbDeviceDescriptor devDesc;
    // 获取设备描述符
    ret = OH_Usb_GetDeviceDescriptor(g_devHandle, &devDesc);
    
    if (ret != 0) {
        OH_LOG_ERROR(LOG_APP, "get dev desc failed:%{public}d", ret);
        return {false, {}};
    }
    // 过滤目的设备
    if (devDesc.idVendor != 0x0b57 || (devDesc.idProduct != 0x8835 && devDesc.idProduct != 0xa153 && devDesc.idProduct != 0xa150 && devDesc.idProduct != 0x8811 && devDesc.idProduct != 0xA13F)) {
        OH_LOG_ERROR(LOG_APP, "device invalid :%{public}u, %{public}u", devDesc.idVendor, devDesc.idProduct);
        return {false, {}};
    }
    return {true, devDesc};
}

static void SetFeature(uint16_t iProduct) {
    OH_LOG_INFO(LOG_APP, "SetFeature enter\n");

    if(iProduct != 0xA150 && iProduct != 0xA153) {
        OH_LOG_WARN(LOG_APP, "no need set feature\n");
        return;
    }

    // 设置feature
    uint32_t len = 100;
    struct UsbControlRequestSetup strDescSetup;
    strDescSetup.bmRequestType = 0x21;
    strDescSetup.bRequest = 0x09;
    strDescSetup.wValue = 0x03 << 8 | 0x02; // desc Index
    strDescSetup.wIndex = 0x0;
    strDescSetup.wLength = 0x02;
    uint8_t data[128] = {0x02, 0x02};
    uint32_t dataLen = 2;
    int32_t ret = OH_Usb_SendControlWriteRequest(g_interfaceHandle, &strDescSetup, UINT_MAX, data, dataLen);
    if(ret < 0) {
        OH_LOG_ERROR(LOG_APP, "OH_Usb_SendControlWriteRequest error ret:%{public}d", ret);
    }
    OH_LOG_INFO(LOG_APP, "SetFeature OK!");
}

static napi_value GetProductDesc(napi_env env, napi_callback_info info) {
    OH_LOG_INFO(LOG_APP, "enter GetProductDesc\n");
    napi_value result;
    // 解析deviceId
    size_t argc = 1;
    napi_value argv;

    napi_get_cb_info(env, info, &argc, &argv, nullptr, nullptr);
    if (argc < 1) {
        OH_LOG_ERROR(LOG_APP, "argc is invalid");
        napi_get_undefined(env, &result);
        return result;
    }

    uint32_t devicedId = 0;
    napi_get_value_uint32(env, argv, &devicedId);
    uint32_t busNum = ((devicedId & 0xFFFF0000) >> 16);
    uint32_t deviceNum = devicedId & 0xFFFF;
    g_devHandle = ((uint64_t)busNum  << 32) | deviceNum;

    auto [res, devDesc] = GetDeviceInfo();
    OH_LOG_INFO(LOG_APP, "GetDeviceInfo ret=:%{public}d\n", res);
    if (!res) {
        napi_get_undefined(env, &result);
        return result;
    }

    if (!GetPipeInfo()) {
        napi_get_undefined(env, &result);
        return result;
    }
    DataParser::GetInstance().Init(devDesc.idVendor, devDesc.idProduct);

    SetFeature(devDesc.idProduct);

    auto productDesc = GetProductStringDescriptor(devDesc.iProduct);
    if (!productDesc.has_value()) {
        OH_LOG_ERROR(LOG_APP, "GetProductStringDescriptor failed");
        napi_get_undefined(env, &result);
        return result;
    }
    return CreateJsArray(env, productDesc.value(), devDesc.idProduct);
}

static napi_value ReleaseResource(napi_env env, napi_callback_info info) {
    OH_LOG_INFO(LOG_APP, "enter ReleaseResource\n");
    stopIo = true;
    OH_Usb_DestroyDeviceMemMap(devMmap);
//     OH_DDK_DestroyAshmem(ashmem);
//     OH_LOG_INFO(LOG_APP, "AAA OH_DDK_DestroyAshmem\n");
    // 释放接口，同时再次将内核的键盘驱动匹配给该键盘
//    OH_LOG_ERROR(LOG_APP, "OH_Usb_ReleaseInterface g_interfaceHandle=%{public}llu", g_interfaceHandle);
    int32_t ret = OH_Usb_ReleaseInterface(g_interfaceHandle);
    if (ret != 0) {
        OH_LOG_ERROR(LOG_APP, "ReleaseInterface failed %{public}d", ret);
    }
    OH_Usb_Release();
    return nullptr;
}

// 设置屏幕分辨率，方便后续坐标标准化
static napi_value SetDisplayInfo(napi_env env, napi_callback_info info) {
    OH_LOG_INFO(LOG_APP, "enter SetDisplayInfo\n");
    size_t argc = 2;
    napi_value args[2] = {nullptr};

    napi_get_cb_info(env, info, &argc, args, nullptr, nullptr);
    if (argc < 2) {
        OH_LOG_ERROR(LOG_APP, "argc is invalid");
        napi_value value;
        return nullptr;
    }

    uint32_t width, height = 0;
    napi_get_value_uint32(env, args[0], &width);
    napi_get_value_uint32(env, args[1], &height);
    OH_LOG_INFO(LOG_APP, "display info width:%{public}u, height:%{public}u", width, height);
    DataParser::GetInstance().SetScreenSize(width, height);
    return nullptr;
}

// 设置按键定制
static napi_value SetButtonMapping(napi_env env, napi_callback_info info) {
    OH_LOG_INFO(LOG_APP, "enter SetButtonMapping\n");
    size_t argc = 1;
    napi_value arg = {nullptr};

    napi_get_cb_info(env, info, &argc, &arg, nullptr, nullptr);
    if (argc < 1) {
        OH_LOG_ERROR(LOG_APP, "argc is invalid");
        napi_value value;
        return nullptr;
    }
    bool isTypedArray = false;
    if (napi_is_typedarray(env, arg, &isTypedArray) != napi_ok || !isTypedArray) {
        OH_LOG_ERROR(LOG_APP, "The type of mappingInfo must be TypedArray.");
        return nullptr;
    }

    napi_typedarray_type type;
    napi_value buffer;
    int32_t *int32Buffer = NULL;
    size_t bufferSize = 0;
    size_t offset = 0;
    napi_status infoStatus = napi_get_typedarray_info(
        env, arg, &type, &bufferSize, reinterpret_cast<void **>(&int32Buffer), &buffer, &offset);
    if (infoStatus != napi_ok) {
        OH_LOG_ERROR(LOG_APP, "get typedarray info failed, status: %{public}d", infoStatus);
        return nullptr;
    }
    if (type != napi_int32_array) {
        OH_LOG_ERROR(LOG_APP, "The type of buffer must be int32Array.");
        return nullptr;
    }
    if (bufferSize == 0 || bufferSize > BTN_MAPPING_INFO_SIZE * sizeof(int32_t)) {
        OH_LOG_ERROR(LOG_APP, "The size of buffer invalid%{public}zu", bufferSize);
        return nullptr;
    }
    DataParser::GetInstance().UpdateKeyCodeMap(0, int32Buffer[0]);
    DataParser::GetInstance().UpdateKeyCodeMap(1, int32Buffer[1]);
    DataParser::GetInstance().UpdateKeyCodeMap(2, int32Buffer[2]);
    DataParser::GetInstance().UpdateKeyCodeMap(3, int32Buffer[3]);
    return nullptr;
}

// 设置按键定制
static napi_value SetOneButtonMapping(napi_env env, napi_callback_info info) {
    OH_LOG_INFO(LOG_APP, "enter SetOneButtonMapping\n");
    size_t argc = 2;
    napi_value args[2] = {nullptr};

    napi_get_cb_info(env, info, &argc, args, nullptr, nullptr);
    if (argc < 2) {
        OH_LOG_ERROR(LOG_APP, "argc is invalid");
        napi_value value;
        return nullptr;
    }

    uint32_t index, keyCode = 0;
    napi_get_value_uint32(env, args[0], &index);
    napi_get_value_uint32(env, args[1], &keyCode);
    DataParser::GetInstance().UpdateKeyCodeMap(index, keyCode);
    return nullptr;
}

// 设置按键定制
static napi_value SetDirection(napi_env env, napi_callback_info info) {
    OH_LOG_INFO(LOG_APP, "enter SetDirection\n");
    size_t argc = 1;
    napi_value args[1] = {nullptr};

    napi_get_cb_info(env, info, &argc, args, nullptr, nullptr);
    if (argc < 1) {
        OH_LOG_ERROR(LOG_APP, "argc is invalid");
        napi_value value;
        return nullptr;
    }

    uint32_t direction = 0;
    napi_get_value_uint32(env, args[0], &direction);
    DataParser::GetInstance().SetDirection(direction);
    return nullptr;
}

// 设置按键定制
static napi_value GetDirection(napi_env env, napi_callback_info info) {
    OH_LOG_INFO(LOG_APP, "enter GetDirection\n");
    uint32_t direction = DataParser::GetInstance().GetDirection();
    napi_value result = nullptr;
    napi_create_int32(env, direction, &result);
    return result;
}

EXTERN_C_START
static napi_value Init(napi_env env, napi_value exports) {
    napi_property_descriptor desc[] = {
        {"getBoardInput", nullptr, GetBoardInput, nullptr, nullptr, nullptr, napi_default, nullptr},
        {"getProductDesc", nullptr, GetProductDesc, nullptr, nullptr, nullptr, napi_default, nullptr},
        {"releaseResource", nullptr, ReleaseResource, nullptr, nullptr, nullptr, napi_default, nullptr},
        {"setDisplayInfo", nullptr, SetDisplayInfo, nullptr, nullptr, nullptr, napi_default, nullptr},
        {"setButtonMapping", nullptr, SetButtonMapping, nullptr, nullptr, nullptr, napi_default, nullptr},
        {"setOneButtonMapping", nullptr, SetOneButtonMapping, nullptr, nullptr, nullptr, napi_default, nullptr},
        {"setDirection", nullptr, SetDirection, nullptr, nullptr, nullptr, napi_default, nullptr},
        {"getDirection", nullptr, GetDirection, nullptr, nullptr, nullptr, napi_default, nullptr},
    };
    napi_define_properties(env, exports, sizeof(desc) / sizeof(desc[0]), desc);
    return exports;
}
EXTERN_C_END

static napi_module demoModule = {
    .nm_version = 1,
    .nm_flags = 0,
    .nm_filename = nullptr,
    .nm_register_func = Init,
    .nm_modname = "entry",
    .nm_priv = ((void *)0),
    .reserved = {0},
};

extern "C" __attribute__((constructor)) void RegisterEntryModule(void) {
    napi_module_register(&demoModule);
}
