//
// Created on 2023/6/29.
//
// Node APIs are not fully supported. To solve the compilation error of the interface cannot be found,
// please include "napi/native_api.h".

#ifndef MyApplication_parse_point_info_H
#define MyApplication_parse_point_info_H
#include <tuple>
std::tuple<bool, uint32_t, uint32_t, uint32_t, bool> ParsePointInfo(uint8_t buffer[], uint32_t length);
#endif //MyApplication_parse_point_info_H
