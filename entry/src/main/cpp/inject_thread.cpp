#include <bits/alltypes.h>
#include <hid/hid_ddk_api.h>
#include <linux/input.h>
#define LOG_TAG "testTag [NATIVE]"

#include "inject_thread.h"
#include "data_parser.h"

#include <hilog/log.h>
std::mutex InjectThread::mutex_;
std::condition_variable InjectThread::conditionVariable_;
std::unordered_map<int32_t, std::vector<Hid_EmitItem>> InjectThread::injectMap_;
std::unordered_map<std::string, int32_t> InjectThread::deviceMap_;

extern "C" {
extern int32_t OH_Hid_CreateDevice(Hid_Device *hidDevice, Hid_EventProperties *hidEventProperties);
extern int32_t OH_Hid_EmitEvent(int32_t deviceId, const Hid_EmitItem items[], uint16_t length);
extern int32_t OH_Hid_DestroyDevice(int32_t deviceId);
}

InjectThread::InjectThread() {
    isRunning_ = true;
}

InjectThread::~InjectThread() {
    isRunning_ = false;
    conditionVariable_.notify_one();
}

void InjectThread::RunThread(void *param) {
    InjectThread *thread = (InjectThread *)param;
    thread->InjectFunc();
}

void InjectThread::Start() {
    thread_ = std::thread(InjectThread::RunThread, this);
    pthread_setname_np(thread_.native_handle(), "emitEvent");
    thread_.detach();
}

void InjectThread::InjectFunc() const {
    std::unique_lock<std::mutex> uniqueLock(mutex_);
    CreateAllDevice();
    while (isRunning_) {
        conditionVariable_.wait(uniqueLock);
        //OH_LOG_INFO(LOG_APP, "OH_Usb_EmitEvent size:%{public}lu deviceId:%{public}d", injectList_.size(), deviceId_);
        for (auto &item : injectMap_) {
            if (item.second.size() == 0) {
                continue;
            }
            //OH_LOG_INFO(LOG_APP, "OH_Usb_EmitEvent type:%{public}d code:%{public}d value:%{public}d", item.type, item.code, item.value);
            OH_Hid_EmitEvent(item.first, item.second.data(), (uint16_t)item.second.size());
        }
        for(auto &item : injectMap_) {
            item.second.clear();
        }
    }

    for (auto &item : injectMap_) {
        OH_LOG_INFO(LOG_APP, "OH_Usb_DestroyDevice deviceId:%{public}d", item.first);
        DestroyDevice(item.first);
        item.second.clear();
    }
    injectMap_.clear();
}

void InjectThread::InjectEvent(std::string deviceName, Hid_EmitItem &injectInputEvent) const {
    std::lock_guard<std::mutex> lockGuard(mutex_);
    auto pos = deviceMap_.find(deviceName);
    if (pos == deviceMap_.end()) {
        OH_LOG_ERROR(LOG_APP, "InjectEvent deviceName:%{public}s not found", deviceName.c_str());
        return;
    }
    injectMap_[pos->second].push_back(injectInputEvent);
}

void InjectThread::Sync() {
    for (auto &item : injectMap_) {
        if (item.second.size() > 0) {
            conditionVariable_.notify_one();
        }
    }
}

void InjectThread::CreateAllDevice() const {
    if (deviceMap_.size() > 0) {
        return;
    }
    int32_t boardId = CreateKeyBoardDevice(DEV_KEYBOARD);
    if (boardId < 0) {
        OH_LOG_INFO(LOG_APP, "CreateKeyBoardDevice fail result:%{public}d", boardId);
        return;
    }
    int32_t deviceId = CreateTouchPadDevice(DEV_TOUCH_PAD);
    if (deviceId < 0) {
        OH_LOG_INFO(LOG_APP, "CreateTouchPadDevice fail result:%{public}d", deviceId);
        DestroyDevice(boardId);
        deviceMap_.clear();
    }
}

void InjectThread::DestroyDevice(int32_t deviceId) const { 
    int32_t res = OH_Hid_DestroyDevice(deviceId);
    OH_LOG_INFO(LOG_APP, "DestroyDevice deviceId:%{public}d, result:%{public}d", deviceId, res);
}

int32_t InjectThread::CreateDevice(Hid_Device *hidDevice, Hid_EventProperties *hidEventProperties) const {
    int deviceId = OH_Hid_CreateDevice(hidDevice, hidEventProperties);
    if (deviceId < 0) {
        OH_LOG_ERROR(LOG_APP, "CreateDevice failed error:%{public}d, deviceName:%{public}s", deviceId,
            hidDevice->deviceName);
    } else {
        deviceMap_.emplace(hidDevice->deviceName, deviceId);
        std::vector<Hid_EmitItem> items;
        injectMap_.emplace(deviceId, items);
        OH_LOG_INFO(LOG_APP, "CreateDevice success deviceId:%{public}d, deviceName:%{public}s", deviceId,
                               hidDevice->deviceName);
    }
    return deviceId;
}

int32_t InjectThread::CreateKeyBoardDevice(std::string deviceName) const {
    Hid_Device hidDevice = {
        .deviceName = deviceName.c_str(), .vendorId = 0x6006, .productId = 0x6008, .version = 1, .bustype = BUS_USB};
    std::vector<Hid_EventType> eventType = {HID_EV_KEY};
    Hid_EventTypeArray eventTypeArray = {.hidEventType = eventType.data(), .length = (uint16_t)eventType.size()};
    std::vector<Hid_KeyCode> keyCode = {
        HID_KEY_1,          HID_KEY_SPACE,       HID_KEY_BACKSPACE,   HID_KEY_ENTER,     HID_KEY_ESC, HID_KEY_SYSRQ,
        HID_KEY_LEFT_SHIFT, HID_KEY_RIGHT_SHIFT, HID_KEY_VOLUME_DOWN, HID_KEY_VOLUME_UP, HID_KEY_0,   HID_KEY_2,
        HID_KEY_3,          HID_KEY_4,           HID_KEY_5,           HID_KEY_6,         HID_KEY_7,   HID_KEY_8,
        HID_KEY_9,          HID_KEY_A,           HID_KEY_B,           HID_KEY_C,         HID_KEY_D,   HID_KEY_E,
        HID_KEY_F,          HID_KEY_G,           HID_KEY_H,           HID_KEY_I,         HID_KEY_J,   HID_KEY_K,
        HID_KEY_L,          HID_KEY_M,           HID_KEY_N,           HID_KEY_O,         HID_KEY_P,   HID_KEY_Q,
        HID_KEY_R,          HID_KEY_S,           HID_KEY_T,           HID_KEY_U,         HID_KEY_V,   HID_KEY_W,
        HID_KEY_X,          HID_KEY_Y,           HID_KEY_Z,           HID_KEY_DELETE};
    Hid_KeyCodeArray keyCodeArray = {.hidKeyCode = keyCode.data(), .length = (uint16_t)keyCode.size()};
    Hid_EventProperties hidEventProp = {.hidEventTypes = eventTypeArray, .hidKeys = keyCodeArray};
    return CreateDevice(&hidDevice, &hidEventProp);
}

int32_t InjectThread::CreateTouchPadDevice(std::string deviceName) const {
    std::vector<Hid_DeviceProp> deviceProp = {HID_PROP_DIRECT};
    Hid_Device hidDevice = {
        .deviceName = deviceName.c_str(), 
        .vendorId = 0x6006, 
        .productId = 0x6006, 
        .version = 1, 
        .bustype = BUS_USB,
        .properties = deviceProp.data(),
        .propLength = (uint16_t)deviceProp.size()
    };
    std::vector<Hid_EventType> eventType = {HID_EV_ABS, HID_EV_KEY, HID_EV_SYN, HID_EV_MSC};
    Hid_EventTypeArray eventTypeArray = {.hidEventType = eventType.data(), .length = (uint16_t)eventType.size()};
    std::vector<Hid_KeyCode> keyCode = {HID_BTN_TOOL_PEN, HID_BTN_TOOL_RUBBER, HID_BTN_TOUCH, HID_BTN_STYLUS, HID_BTN_RIGHT};
    Hid_KeyCodeArray keyCodeArray = {.hidKeyCode = keyCode.data(), .length = (uint16_t)keyCode.size()};
    std::vector<Hid_MscEvent> mscEvent = {HID_MSC_SCAN};
    Hid_MscEventArray mscEventArray = {.hidMscEvent = mscEvent.data(), .length = (uint16_t)mscEvent.size()};
    std::vector<Hid_AbsAxes> absAxes = {HID_ABS_X, HID_ABS_Y, HID_ABS_PRESSURE};
    Hid_AbsAxesArray absAxesArray = {.hidAbsAxes = absAxes.data(), .length = (uint16_t)absAxes.size()};
    Hid_EventProperties hidEventProp = { 
        .hidEventTypes = eventTypeArray, 
        .hidKeys = keyCodeArray, 
        .hidAbs = absAxesArray,
        .hidMiscellaneous=mscEventArray
    };
    int32_t maxX, maxY, maxPressure;
    DataParser::GetInstance().GetAbsMax(maxX, maxY, maxPressure);
    OH_LOG_INFO(LOG_APP, "OH_Usb_CreateDevice maxX:%{public}d, maxY:%{public}d, maxPressure:%{public}d", maxX, maxY,
                maxPressure);
    hidEventProp.hidAbsMin[HID_ABS_X] = 0;
    hidEventProp.hidAbsMin[HID_ABS_Y] = 0;
    hidEventProp.hidAbsMin[HID_ABS_PRESSURE] = 0;
    hidEventProp.hidAbsMax[HID_ABS_X] = maxX;
    hidEventProp.hidAbsMax[HID_ABS_Y] = maxY;
    hidEventProp.hidAbsMax[HID_ABS_PRESSURE] = maxPressure;
    return CreateDevice(&hidDevice, &hidEventProp);
}