#ifndef INJECT_THREAD_H
#define INJECT_THREAD_H

#include <mutex>
#include <thread>
#include "hid/hid_ddk_types.h"

const std::string DEV_KEYBOARD = "VSoC keyboard";
const std::string DEV_TOUCH_PAD = "VSoC touchpad";

class InjectThread {
  public:
    InjectThread();
    virtual ~InjectThread();
    void Start();
    void InjectEvent(std::string deviceName, Hid_EmitItem &injectInputEvent) const;
    void Sync();
    int32_t CreateKeyBoardDevice(std::string deviceName) const;
    int32_t CreateTouchPadDevice(std::string deviceName) const;
    void DestroyDevice(int32_t deviceId) const;
    void CreateAllDevice() const;

private:
    static void RunThread(void *param);
    void InjectFunc() const;
    int32_t CreateDevice(Hid_Device *hidDevice, Hid_EventProperties *hidEventProperties) const;

private:
    static std::mutex mutex_;
    static std::condition_variable conditionVariable_;
    static std::unordered_map<int32_t, std::vector<Hid_EmitItem>> injectMap_;
    static std::unordered_map<std::string, int32_t> deviceMap_;
    bool isRunning_;
    std::thread thread_;
};

#endif // INJECT_THREAD_H