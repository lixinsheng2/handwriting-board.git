declare namespace testNapi {
  function getProductDesc(deviceId: number):[string, number];
  function getBoardInput():Promise<void>;
  function releaseResource():void;
  function setDisplayInfo(width:number, height:number):void;
  function setButtonMapping(mappingInfo: Int32Array):void;
  function setOneButtonMapping(index:number, value:number):void;
  function setDirection(direction:number):void;
  function getDirection():number;
}
export default testNapi;